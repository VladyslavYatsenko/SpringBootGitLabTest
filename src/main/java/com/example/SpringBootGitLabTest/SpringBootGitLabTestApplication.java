package com.example.SpringBootGitLabTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGitLabTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGitLabTestApplication.class, args);
	}

}
