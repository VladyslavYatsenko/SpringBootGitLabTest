package com.example.SpringBootGitLabTest.service.impl;

import com.example.SpringBootGitLabTest.service.MathService;
import org.springframework.stereotype.Service;

@Service
public class MathServiceImpl implements MathService {
    @Override
    public double add(double a, double b) {
        return a+b;
    }

    @Override
    public double remove(double a, double b) {
        return a-b;
    }

    @Override
    public double multiply(double a, double b) {
        return a*b;
    }

    @Override
    public double division(double a, double b) {
        return a/b;
    }
}
