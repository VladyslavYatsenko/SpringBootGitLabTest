package com.example.SpringBootGitLabTest.controller;

import com.example.SpringBootGitLabTest.service.MathService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class MathController {

    private MathService mathService;

    @PostMapping("/add/{a}/{b}")
    public double add(@PathVariable(value = "a") double a,
                      @PathVariable(value = "b") double b) {
        return mathService.add(a, b);

    }

    @PostMapping("/remove/{a}/{b}")
    public double remove(@PathVariable(value = "a") double a,
                         @PathVariable(value = "b") double b) {
        return mathService.remove(a, b);

    }

    @PostMapping("/multiply/{a}/{b}")
    public double multiply(@PathVariable(value = "a") double a,
                           @PathVariable(value = "b") double b) {
        return mathService.multiply(a, b);

    }

    @PostMapping("/divide/{a}/{b}")
    public double divide(@PathVariable(value = "a") double a,
                         @PathVariable(value = "b") double b) {
        return mathService.division(a, b);

    }
}
