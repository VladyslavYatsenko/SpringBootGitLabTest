package com.example.SpringBootGitLabTest.service.impl;

import com.example.SpringBootGitLabTest.service.MathService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MathServiceImplTest {

    private MathService mathService;
    @Before
    public void init(){
        mathService=new MathServiceImpl();
    }

    @Test
    public void shouldAddTwoNumbers(){
        double expected=8.0;
        assertEquals(expected,mathService.add(5,3),0);
    }
    @Test
    public void shouldDivideTwoNumbers(){
        double expected=2;
        assertEquals(expected,mathService.division(4,2),0);
    }

    @Test
    public void shouldMultiplyTwoNumbers(){
        double expected=25;
        assertEquals(expected,mathService.multiply(5,5),0);
    }

    @Test
    public void shouldFailOnWrongAdding(){
        double expected=8;
        assertNotEquals(expected, mathService.add(5, 2), 0.0);
    }

    @Test
    public void shouldSubtractTwoNumbers(){
        assertEquals(2,mathService.remove(4,2),0);
    }




}